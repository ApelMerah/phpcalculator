<?php

namespace Jakmall\Recruitment\Calculator\History\Infrastructure;

//TODO: create implementation.
interface CommandHistoryManagerInterface
{
    /**
     * Returns array of command history.
     *
     * @param mixed $driver The option of file or database (default=database).
     * @return array
     */
    public function findAll($driver): array;

    /**
     * Log command data to storage.
     *
     * @param mixed $driver The option of file or database (default=both).
     * @param mixed $command The command to log.
     *
     * @return bool Returns true when command is logged successfully, false otherwise.
     */
    public function log($driver, $command): bool;

    /**
     * Clear all data from storage.
     *
     * @param mixed $driver The option of file or database (default=both).
     * @return bool Returns true if all data is cleared successfully, false otherwise.
     */
    public function clearAll($driver):bool;
}
