<?php

namespace Jakmall\Recruitment\Calculator\History\Infrastructure;

use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;
use Illuminate\Database\Capsule\Manager as Capsule;
use Jakmall\Recruitment\Calculator\Models\History;
use Illuminate\Support\Str;

class CommandHistoryManager implements CommandHistoryManagerInterface
{
    public function findAll($driver): array
    {
        $arr = [];
        $index = 0;
        if ($driver === "file") {
            $file = fopen("history.log", "r") or die ("Unable to open file!");

            while (!feof($file)) {
                array_push($arr, explode("#", fgets($file)));
                array_unshift($arr[$index], ++$index);
            }
            array_pop($arr);

            fclose($file);
        } elseif ($driver === "database") {
            $history = new History;
            $historyResult = $history::all();
            foreach ($historyResult as $data) {
                array_push($arr, []);
                array_push($arr[$index], $index + 1);
                array_push($arr[$index], $data->command);
                array_push($arr[$index], $data->description);
                array_push($arr[$index], $data->result);
                array_push($arr[$index], $data->output);
                array_push($arr[$index], $data->time);
                $index++;
            }
        }

        return $arr;
    }

    public function log($driver, $command): bool
    {
        $id = (string) Str::uuid();
        $command = $id . "#" . $command;

        if ($driver === "file" || $driver === "both") {
            // File driver
            $file = fopen("history.log", "a");
            fwrite($file, $command);
            fclose($file);
        }

        if ($driver === "database" || $driver === "both") {
            // Database driver
            $arr = [];
            $command = str_replace("\n", "", $command);
            array_push($arr, explode("#", $command));
            $arr = $arr[0];

            $history = new History;
            $history->fill([
                'id' => $arr[0],
                'command' => $arr[1],
                'description' => $arr[2],
                'result' => $arr[3],
                'output' => $arr[4],
                'time' => $arr[5]
                ]);
            $history->save();
        }

        return true;
    }

    public function clearAll($driver): bool
    {

        if ($driver === "file" || $driver === "both") {
            // File driver
            $file = fopen("history.log", "w");
            fwrite($file, "");
            fclose($file);
        }

        if ($driver === "database" || $driver === "both") {
            // Database driver
            $history = new History;
            $history::truncate();
        }
        return true;
    }
}
