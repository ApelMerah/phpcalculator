<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Jakmall\Recruitment\Calculator\Commands\AddCommand;
use Jakmall\Recruitment\Calculator\Commands\SubtractCommand;
use Jakmall\Recruitment\Calculator\Commands\MultiplyCommand;
use Jakmall\Recruitment\Calculator\Commands\DivideCommand;
use Jakmall\Recruitment\Calculator\Commands\PowCommand;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManager;

class CalculatorController
{
    protected $addCommandMan = null;
    protected $subtractCommandMan = null;
    protected $multiplyCommandMan = null;
    protected $divideCommandMan = null;
    protected $powCommandMan = null;

    public function __construct(
        AddCommand $addCommand,
        SubtractCommand $subtractCommand,
        MultiplyCommand $multiplyCommand,
        DivideCommand $divideCommand,
        PowCommand $powCommand
        )
    {
        $this->addCommandMan = $addCommand;
        $this->subtractCommandMan = $subtractCommand;
        $this->multiplyCommandMan = $multiplyCommand;
        $this->divideCommandMan = $divideCommand;
        $this->powCommandMan = $powCommand;
    }

    public function calculate(Request $req, $action)
    {
        $input = json_decode($req->input('input'));

        $action = strtolower($action);

        if ($action === "add") {
            $response = $this->addCommandMan->getCalculationResult($input);
        } else if ($action === "subtract") {
            $response = $this->subtractCommandMan->getCalculationResult($input);
        } else if ($action === "multiply") {
            $response = $this->multiplyCommandMan->getCalculationResult($input);
        } else if ($action === "divide") {
            $response = $this->divideCommandMan->getCalculationResult($input);
        } else if ($action === "pow") {
            $response = $this->powCommandMan->getCalculationResult($input);
        } else {
            $response = "Action not found!";
        }

        return json_encode($response);
    }
}
