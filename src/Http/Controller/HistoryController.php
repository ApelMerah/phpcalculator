<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Jakmall\Recruitment\Calculator\Models\History;

class HistoryController
{
    public function index()
    {
        $history = History::all();
        foreach($history as $index => $data) {
            $result[$index]["id"] = $data->id;
            $result[$index]["command"] = strtolower($data->command);
            $result[$index]["operation"] = $data->description;

            if ($data->command === "Add") {
                $operator = '+';
            } else if ($data->command === "Subtract") {
                $operator = '-';
            } else if ($data->command === "Multiply") {
                $operator = '*';
            } else if ($data->command === "Divide") {
                $operator = '/';
            } else if ($data->command === "Pow") {
                $operator = '^';
            }
            $operationWithoutSpace = str_replace(" ", "", $data->description);
            $result[$index]["input"] = explode($operator, $operationWithoutSpace);
            $result[$index]["result"] = $data->result;
            $result[$index]["time"] = $data->time;
        }
        return json_encode($result);
    }

    public function show(Request $req, $id)
    {
        $data = History::where("id", $id)->select("id", "command", "description", "result", "time")->first();
        $result["id"] = $data->id;
        $result["command"] = strtolower($data->command);
        $result["operation"] = $data->description;

        if ($data->command === "Add") {
            $operator = '+';
        } else if ($data->command === "Subtract") {
            $operator = '-';
        } else if ($data->command === "Multiply") {
            $operator = '*';
        } else if ($data->command === "Divide") {
            $operator = '/';
        } else if ($data->command === "Pow") {
            $operator = '^';
        }
        $operationWithoutSpace = str_replace(" ", "", $data->description);
        $result["input"] = explode($operator, $operationWithoutSpace);
        $result["result"] = $data->result;
        $result["time"] = $data->time;
        return json_encode($result);
    }

    public function remove(Request $req, $id)
    {
        // File driver
        // Get all data from file
        $arr = [];
        $index = 0;
        $file = fopen("history.log", "r") or die ("Unable to open file!");

        while (!feof($file)) {
            array_push($arr, explode("#", fgets($file)));
            array_unshift($arr[$index], ++$index);
        }
        array_pop($arr);

        fclose($file);

        foreach($arr as $indexData => $data) {
            if ($data[1] == $id) {
                array_splice($arr, $indexData, 1);
                break;
            }
        }

        // Rewrite empty the file
        $file = fopen("history.log", "w");
        fwrite($file, '');
        fclose($file);

        $file = fopen("history.log", "a");
        foreach($arr as $data) {
            $result = "";
            foreach ($data as $index => $item) {
                if ($index > 0) {
                    $result = $result . ($index == 1 ? '' : '#') . $item;
                }
            }
            // Add data after removal
            fwrite($file, $result);
        }
        fclose($file);

        // Database driver
        $history = new History();
        $deleteTarget = $history->where('id', $id);
        if ($deleteTarget) {
            $deleteTarget->delete();
        }

        return new Response('', 204);
    }
}
