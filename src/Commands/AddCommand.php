<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Jakmall\Recruitment\Calculator\History\CommandHistoryManager;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class AddCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = "add {numbers* : The numbers to be added} {--driver=both : Driver for storage connection}";

    /**
     * @var string
     */
    protected $description = "Add all given Numbers";

    protected $commandHistoryManager = null;

    public function __construct(CommandHistoryManagerInterface $chm)
    {
        parent::__construct();
        $this->commandHistoryManager = $chm;
    }

    public function handle(): void
    {
        $numbers = $this->getInput();
        $description = $this->generateCalculationDescription($numbers);
        $result = $this->calculateAll($numbers);

        $output = $description . " = " . $result;

        $this->comment(sprintf($output));

        $command = "Add";
        $time = Carbon::now()->toDateTimeString();

        $fileSave = $command . "#" . $description . "#" . $result . "#" . $output . "#" . $time . "\n";

        $this->commandHistoryManager->log($this->option('driver'), $fileSave);
    }

    public function getCalculationResult($input)
    {
        $numbers = $input;
        $description = $this->generateCalculationDescription($numbers);
        $result = $this->calculateAll($numbers);

        $output = $description . " = " . $result;

        $command = "Add";
        $time = Carbon::now()->toDateTimeString();

        $fileSave = $command . "#" . $description . "#" . $result . "#" . $output . "#" . $time . "\n";

        $this->commandHistoryManager->log('both', $fileSave);

        $data = (object) [
            'command' => $command,
            'operation' => $description,
            'result' => $result
        ];

        return $data;
    }

    protected function getInput(): array
    {
        return $this->argument('numbers');
    }

    protected function generateCalculationDescription(array $numbers): string
    {
        $operator = $this->getOperator();
        $glue = sprintf(' %s ', $operator);

        return implode($glue, $numbers);
    }

    protected function getOperator(): string
    {
        return '+';
    }

    /**
     * @param array $numbers
     *
     * @return float|int
     */
    protected function calculateAll(array $numbers)
    {
        $number = array_pop($numbers);

        if (count($numbers) <= 0) {
            return $number;
        }

        return $this->calculate($this->calculateAll($numbers), $number);
    }

    /**
     * @param int|float $number1
     * @param int|float $number2
     *
     * @return int|float
     */
    protected function calculate($number1, $number2)
    {
        return $number1 + $number2;
    }
}
