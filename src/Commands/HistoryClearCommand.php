<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\CommandHistoryManager;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class HistoryClearCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = "history:clear {--driver=both : Driver for storage connection}";

    /**
     * @var string
     */
    protected $description = "Clear saved history";

    protected $commandHistoryManager = null;

    public function __construct(CommandHistoryManagerInterface $chm)
    {
        parent::__construct();
        $this->commandHistoryManager = $chm;
    }

    public function handle(): void
    {
        $data = $this->commandHistoryManager->clearAll($this->option('driver'));
        $this->info("History cleared!");
    }
}
