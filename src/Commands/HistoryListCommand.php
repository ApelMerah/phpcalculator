<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\CommandHistoryManager;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class HistoryListCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = "history:list {commands?* : filter the history by commands} {--driver=database : Driver for storage connection}";

    /**
     * @var string
     */
    protected $description = "Show calculator history";

    protected $commandHistoryManager = null;

    public function __construct(CommandHistoryManagerInterface $chm)
    {
        parent::__construct();
        $this->commandHistoryManager = $chm;
    }

    public function handle(): void
    {
        $data = $this->commandHistoryManager->findAll($this->option("driver"));

        if (count($data) > 0) {
            $headers = ['No', 'Command', 'Description', 'Result', 'Output', 'Time'];
            $this->table($headers, $data);
        } else {
            $this->info("History is empty.");
        }
    }
}
