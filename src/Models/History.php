<?php

namespace Jakmall\Recruitment\Calculator\Models;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }

    public $timestamps = false;
    protected $fillable = ['id', 'command', 'description', 'result', 'output', 'time'];
    protected $guarded = [];
}
