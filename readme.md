# Jakmall Calculator

## Software Requirements
- Docker

## Vendor installation
```
./composer install
```
## Setup database
```
config your database from database.php

sudo php create-table.php
```
## Run the Calculator
```
./calculator
```
