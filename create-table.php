<?php

require __DIR__.'/vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('histories', function($table) {
    $table->uuid('id')->primary();
    $table->string('command');
    $table->string('description');
    $table->string('result');
    $table->string('output');
    $table->string('time');
});
